import random
from PIL import Image
avit = []

try:
	size = int(input("Size: "))
except ValueError:
	print("Input is not int, defaulting to 8")
	size = 8
size = size + int(size % 2)

avit.extend([random.choice([1,0]) for I in range(0, size // 2)] for i in range(0, size))

for i in range(0,size):
	avit[i].extend([I for I in reversed(avit[i])])

color = {0: (random.randint(5, 240), random.randint(5, 240), random.randint(5, 240)), 1: (255, 255, 255)}
avitIma = Image.new("RGBA", (size,size))

for i in range(0, len(avit)):
	for I in range(0, len(avit[i])):
		avitIma.putpixel((I, i), color[avit[i][I]])


avitIma = avitIma.resize((max(1000, size),max(1000, size)), Image.NEAREST)
avitIma.save("avitar.png")